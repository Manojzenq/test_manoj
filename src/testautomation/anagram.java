package testautomation;

import java.util.Arrays;
import java.util.Scanner;


public class anagram {

	public static void main(String[] args) {
		
		try (Scanner s = new Scanner(System.in)) {
			String s1=s.nextLine();
			String s2=s.nextLine();
			if(s1.length()==s2.length()){
			    char[] c1=s1.toCharArray();
			    char[] c2=s2.toCharArray();
			    Arrays.sort(c1);
			    Arrays.sort(c2);
			   
			    if(Arrays.equals(c1,c2)){
			        System.out.println("anagram");
			        
			    }
			    else{
			        System.out.println("not anagram");
			    }
			}
		
		
		}

	}

}
